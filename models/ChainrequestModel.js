const db = require('../database/db-connection');
const { multipleColumnSet,multipleColumnUpdate } = require('../utils/common.utils');


class ChainRequestModel {
  tableName = "chainrequest";

  getAll = async () => {
    let sql = `SELECT * FROM ${this.tableName}`;
    return await db.query(sql);
  };

  findOne = async (params) => {
    const { columnSet, values } = multipleColumnSet(params);

    const sql = `SELECT * FROM ${this.tableName}
        WHERE ${columnSet}`;

    const result = await db.query(sql, [...values]);

    return result;
  };

  updateChain = async (params, id) => {
    const { columnSet, values } = multipleColumnUpdate(params);

    const sql = `UPDATE ${this.tableName} SET ${columnSet} WHERE id = ?`;

    const result = await db.query(sql, [...values, id]);

    return result;
  };

  getChainFriends = async (params) => {
    const { columnSet, values } = multipleColumnSet(params);

    // const sql = `SELECT * FROM ${this.tableName}
    //     WHERE ${columnSet} and ${this.tableName}.status = true`;

    const sql = `SELECT chainrequest.*,
    users.username,
    users.email,
    users.phone_number
    FROM ${this.tableName}, users
    WHERE chainrequest.user_id = users.id and ${columnSet}
    `;
    console.log(sql);
    const result = await db.query(sql, [...values]);

    return result;
  };

  updateChainrequest = async (params, id) => {
    const { columnSet, values } = multipleColumnUpdate(params);

    const sql = `UPDATE ${this.tableName} SET status=true WHERE id = ?`;

    const result = await db.query(sql, [...values, id]);

    return this.findOne({
      id: Number(req.params.id),
    });
  };

  deleteChainrequest = async (params, id) => {
    const { columnSet, values } = multipleColumnUpdate(params);

    const sql = `DELETE FROM ${this.tableName} WHERE ${columnSet}`;

    const result = await db.query(sql, [...values, id]);

    const affectedRows = result ? result.affectedRows : 0;

    return affectedRows;
  };
  deleteAllChainrequests = async () => {
    const sql = `DELETE FROM ${this.tableName} `;

    const result = await db.query(sql);

    const affectedRows = result ? result.affectedRows : 0;

    return affectedRows;
  };

  addChain = async ($data) => {
    const sql = `INSERT INTO ${this.tableName}  SET ? `;

    const result = await db.query(sql, $data);
    const affectedRows = result ? result.affectedRows : 0;

    return this.findOne({
      userid: $data.userid,
      chainuserid: $data.chainuserid,
    });
  };

  getUser = async ($data) => {
    const sql = `SELECT * FROM ${this.tableName} \
                WHERE userid= ${$data.userid} AND status=true\
                UNION\
                SELECT * FROM ${this.tableName} \
                WHERE chainuserid= ${$data.chainuserid} AND status=true`;

    const result = await db.query(sql, [...$data]);

    return result;
  };
  getfriendslist = async ($data) => {
   
    const sql =`SELECT users.id as userid,users.username,users.email,user_image.name from users \
               LEFT JOIN user_image ON users.id=user_image.id \
               WHERE users.id IN \
              (SELECT chainrequest.chainuserid as userid \
                FROM chainrequest WHERE userid=${$data.userid} and  status=1 \
              UNION \
              SELECT chainrequest.userid as userid \
               FROM chainrequest WHERE chainuserid=${$data.userid} and  status=1)`

    const result = await db.query(sql);

    return result;
  };

  getchainlist = async (params) => {
    const { columnSet, values } = multipleColumnUpdate(params);

    const sql = `SELECT chainrequest.id as chainid, chainrequest.status,\
	                chainrequest.chainuserid,chainrequest.userid,\
		              users.username,users.email, users.phone_number,user_image.name\
                FROM\
                      ${this.tableName}, users\
                left join user_image on\
		                  users.id=user_image.id \
                  WHERE ${columnSet} and users.id = chainrequest.userid and 
                  chainrequest.status = 0`;

    const result =  await db.query(sql,[...values]);
    return result;
  };
  // LEFT JOIN users ON
  //       chainrequest.userid = users.id

  //  ${this.tableName},solution, users
  //   LEFT JOIN user_image ON
  //       users.id=user_image.id
  //   WHERE ${columnSet} and users.id= solutionshare.user_id
  // and solution.solution_id=solutionshare.solution_id`;

  // getFriends= async ($data)=>{
  //   const sql = `SELECT chainrequest.*, FROM ${this.tableName} \
  //               WHERE userid= ${$data.userid} AND status=true\
  //               UNION\
  //               SELECT * FROM ${this.tableName} \
  //               WHERE chainuserid= ${$data.chainuserid} AND status=true\
  //               LEFT JOIN

  //               `;
  // }
}
module.exports = new ChainRequestModel();