const express = require('express');
const bodyParser = require('body-parser');
const cors = require("cors");
const HttpException = require("./utils/HttpException");

const app = express();
const port = 8202;


app.use(cors());
app.options("*", cors());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(function(req, res, next) {
  
    console.log(req.path)
     
    next();
  });


app.use('/', require('./routes'));

app.all('*', (req, res, next) => {
    const err = new HttpException(404, 'Endpoint Not Found');
    next(err);
});

app.listen(port, () => {
  console.log(`server listing at port:${port}`)
});

module.exports = app;