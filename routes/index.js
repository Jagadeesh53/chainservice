var express = require("express");
var router = express.Router();

var ChainrequestManager = require('../controllers/ChainrequestController')

router.get("/api/getAllChain", ChainrequestManager.getAllChain);
router.get("/api/chainid/:id", ChainrequestManager.getOne);
router.get("/api/updateChain/:id", ChainrequestManager.updateChain);
router.get("/api/rejectChain/:id", ChainrequestManager.rejectChain);
// router.get("/api/MyFriendList/:chainuserid", ChainrequestManager.getchainfriends);
router.get("/api/getchainlist/:chainuserid", ChainrequestManager.getchainlist); 
router.put("/api/chainrequest/:id", ChainrequestManager.updateChainrequest);
router.delete("/api/deletechain/:id", ChainrequestManager.deleteChainrequest);
router.delete("/api/deleteAllchain", ChainrequestManager.deleteAllChainrequests);
router.get("/api/chainrequest/:id", ChainrequestManager.findByStatus);
// router.get("/api/users", ChainrequestManager.getUser);
router.post("/api/add", ChainrequestManager.createChain);
router.get("/api/getUser/:userid", ChainrequestManager.getUser);
// router.get("/api/MyFrnds/:userid", ChainrequestManager.getfriends);
router.get("/api/getMyfriendslist/:userid", ChainrequestManager.getfriendslist);


module.exports = router;
