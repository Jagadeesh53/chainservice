var caseConversion = require("../services/caseConversion");
var ChainRequestModel = require("../models/ChainrequestModel");

var ChainRequestController = {
  getAllChain: async function (req, res) {
    let result = await ChainRequestModel.getAll();

    if (result.length >= 0) {
      res.status(200).send(result);
    } else {
      res.status(400).send(result);
    }
  },

  getOne: async function (req, res) {
    let chainrequestdata = await ChainRequestModel.findOne({
      id: Number(req.params.id),
    });
    if (chainrequestdata) {
      res.status(200).send(chainrequestdata);
    } else {
      res.status(404);
    }
  },

  updateChain: async function (req, res) {
    var chainrequestdata = await ChainRequestModel.findOne({
      id: Number(req.params.id),
    });
    if (chainrequestdata) {
      let result = await ChainRequestModel.updateChain({ status: true }, Number(req.params.id));
      res.status(200).send({ ...chainrequestdata });
    } else {
      res.status(404);
    }
  },

  rejectChain: async function (req, res) {
    var chainrequestdata = await ChainRequestModel.findOne({
      id: Number(req.params.id),
    });
    if (chainrequestdata) {
      let result = await ChainRequestModel.updateChain({ status: false }, id);
      res.status(200).send({ ...chainrequestdata });
    } else {
      res.status(404);
    }
  },

  getchainfriends: async function (req, res) {
    let chainrequestdata = await ChainRequestModel.getChainFriends({
      chainuserid: Number(req.params.chainuserid),
      status: false,
    });
    if (chainrequestdata) {
      res.status(200).send(chainrequestdata);
    } else {
      res.status(404);
    }
  },

  updateChainrequest: async function (req, res) {
    let chainrequestdata = await ChainRequestModel.updateChainrequest({
      id: Number(req.params.id),
    });
    if (chainrequestdata) {
      res.status(200).send(chainrequestdata);
    } else {
      res.status(404);
    }
  },

  deleteChainrequest: async function (req, res) {
    let result = await ChainRequestModel.deleteChainrequest({
      chainid: Number(req.params.id),
    });
    if (result > 0) {
      res.status(200);
    } else {
      res.status(404);
    }
  },

  deleteAllChainrequests: async function (req, res) {
    let result = await ChainRequestModel.deleteAllChainrequests();
    if (result > 0) {
      res.status(200);
    } else {
      res.status(404);
    }
  },

  findByStatus: async function (req, res) {
    let chainrequestdata = await ChainRequestModel.findOne({
      status: true,
    });
    if (chainrequestdata.length > 0) {
      res.status(200).send(chainrequestdata);
    } else if (chainrequestdata.length == 0) {
      res.status(200).send(chainrequestdata);
    } else {
      res.status(404);
    }
  },

  createChain: async function (req, res) {
    let dataExists = await ChainRequestModel.findOne({
      userid: req.body.userid,
      chainuserid: req.body.chainuserid,
    });

    if (dataExists.length > 0) {
      res.status(400).send({ message: "friend request taken already" });
    } else {
      let result = await ChainRequestModel.addChain([req.body]);

      res.status(200).send({
        message: "friend request Sending successfully " + result.status,
      });
    }
  },

  getUser: async function (req, res) {
    let result = await ChainRequestModel.getUser({
      userid: req.params.userid,
      chainuserid: req.params.userid,
    });

    if (result) {
      res.status(200).send(result);
    } else {
      res.status(400).send(result);
    }
  },
  getfriendslist:async function (req, res) {
    let result = await ChainRequestModel.getfriendslist({
      userid: req.params.userid,
    
    });

    if (result) {
      res.status(200).send(result);
    } else {
      res.status(400).send(result);
    }
  },

  getchainlist: async function(req, res) {
    let result = await ChainRequestModel.getchainlist({
      chainuserid: req.params.chainuserid
    });

    for (var i = 0; i < result.length; i++) {
      // console.log("154", typeof result[i].status)
      // console.log("155", Buffer.from(result[i].status));
      // console.log("156", Buffer.from(result[i].status).readInt8());
      if (Buffer.from(result[i].status).readInt8() == 0) {
        result[i].status = false;
      }
    }
    console.log("158", result)
    res.status(200).send(result);
  }
  // getfriends: async function (req, res) {
  //   let result = await ChainRequestModel.getFriends({
  //     userid: req.params.userid,
  //     chainuserid: req.params.userid,
  //   });

  //   if (result) {
  //     res.status(200).send(result);
  //   } else {
  //     res.status(400).send(result);
  //   }
  // },
};


module.exports = ChainRequestController;