var otpGenerator = require('otp-generator')

var passwordGenerator ={
     generatePassword:function(){
       let password = otpGenerator.generate(8, { upperCase:true, specialChars: true,alphabets:true,digits:true });
      // console.log(password)
      return password;
     }
     
}
    //passwordGenerator.generatePassword()
module.exports = passwordGenerator;