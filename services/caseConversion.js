var caseConversion = {
    camelToUnderscore(obj) {
        let newObject = {};
        for (item in obj) {
            newObject[this.convertToSnakeCase(item)] = obj[item];
        }
        return newObject;
    },

    convertToSnakeCase(key) {
        return key.replace(/([A-Z])/g, "_$1").toLowerCase();
    },
    underScoreToCamelCase(obj) {
        let newObject = {};
        for (item in obj) {
            newObject[this.convertToCamelCase(item)] = obj[item];
        }
        return newObject;
    },

    convertToCamelCase(key) {
        return key.toLowerCase().replace(/([_][a-z])/g, group =>
        group
          .toUpperCase()
          .replace('_', ''));
    //     str.toLowerCase().replace(/([-_][a-z])/g, group =>
    //     group
    //       .toUpperCase()
    //       .replace('-', '')
    //       .replace('_', '')
    //   );
    }
}

module.exports=caseConversion;

